# Muznizar 🎧


> This app for downloading video from youtube and convert it to mp3 then split it and at last use API to recognize the artist.


## Installation 🛠

**1- [youtube-dllink](https://github.com/ytdl-org/youtube-dl/blob/master/README.md#readme)**

`brew install youtube-dl`

**2- [ffmpeg](https://ffmpeg.org/)**

`brew install ffmpeg`

**3- Run scripts**

`npm start`

`npm run socket`

**4- Click [here](http://localhost:3000)**

----
### TODO 📝:
1. Use youtube search api to make a playlist
2. Complete socket
3. Use docker
