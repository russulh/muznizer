var createError = require('http-errors');
var express = require('express');
var router = express.Router();
const path = require('path');
const recognizerController = require('../app/controllers/recognizerController')
/* GET home page. */

router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Music Recognizer',
    by: 'Russulh'
  });
});

router.get('/test', (req, res, next) => {
  recognizerController.deleteFiles();
});

router.post('/search', async (req, res, next) => {
  if (!req.body.url) return next(createError(406));

  try {
    await recognizerController.deleteFiles();
    let result = await recognizerController.handle(req.body.url);
    return res.json(result);
  } catch (e) {
    return next(createError(500));
  }
});

module.exports = router;