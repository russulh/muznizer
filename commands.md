## Useful shell Commands 📟:-
### youtube-dl
1. download and convert it to mp3

    `youtube-dl -x --audio-format mp3 https://youtu.be/8tbP3f3i03E`

2. download and convert it to mp3 and save to your storage directory

    `youtube-dl -x --audio-format mp3 -o 'storage/%(title)s.%(ext)s' url`

### ffmpeg

1. convert and split ss(starting time to split) -t duration

    `ffmpeg -ss 15 -t 30 -i input_file.mp4 output_file.mp3`
