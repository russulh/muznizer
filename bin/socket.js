const express = require('express');
const app = express();
var http = require('http');
const server = http.createServer(app);
const colors = require('colors');

const io = require("socket.io")(server);

require('dotenv').config();

let data = []

io.on("connection", (socket) => {
  //Socket is a Link to the Client 
  console.log("New Client is Connected!", socket.id);
  socket.on('data', async function (name) {
    console.log('this from socket ' + name);
  });

  socket.on('disconnect', function () {
    console.log('Client disconnected', socket.id);
  });
});

server.listen(process.env.SKT_PORT, process.env.SRV_LISTEN);
server.on('listening', function () {
  console.log(colors.bgMagenta.bold.underline(`Server is running on http://localhost:${process.env.SKT_PORT}`));
});