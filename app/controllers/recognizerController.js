require('dotenv').config()
const util = require('util');
const exe = util.promisify(require('child_process').exec);
const fs = require('fs');
const request = require('request');
const path = require('path');

const io = require("socket.io-client")
const socket = io(`http://localhost:${process.env.SKT_PORT}`)

/* todo: 
      1- return understandable errors
      2- use socket to change the status 
*/

function deleteFiles() {
   console.log('deleteFiles');
   socket.emit('data', 'russulh');
   return new Promise((resolve, reject) => {
      let dir_path = path.join(__dirname, '../../' + process.env.APP_STORAGE);
      try {
         if (fs.existsSync(dir_path)) {
            if (fs.readdirSync(dir_path).length > 0) {
               fs.readdirSync(dir_path).forEach(function (file, index) {
                  var curPath = dir_path + "/" + file;
                  fs.unlinkSync(curPath);
               });
            }
            fs.rmdirSync(dir_path);
         }
         resolve(true);
      } catch (error) {
         reject(false);
      }
   });
}

function handle(url) {
   console.log('handle');

   return new Promise(async (resolve, reject) => {
      try {
         const {
            stdout,
            stderr
         } = await exe(`youtube-dl --dump-json --skip-download ${url}`);
         // console.log(stdout, stderr);
         var info = JSON.parse(stdout);
         var name = await SHYoutubeDl(url);
         await SHFfmpeg(name);
         var artist_name = await recognizeByAuddIO(name);

         var data = {
            title: info.title,
            link: info.webpage_url,
            thumbnail: info.thumbnail,
            artist: artist_name
         };

         return resolve(data);
      } catch (error) {
         console.error("%%", error);
         return reject(error);
      }
   });
}

function SHYoutubeDl(url) {
   console.log('SHYoutubeDl');

   return new Promise(async (resolve, reject) => {
      let name = '';
      try {
         const {
            stdout,
            stderr
         } = await exe(`youtube-dl -x --audio-format mp3 -o '${process.env.APP_STORAGE}/%(id)s.%(ext)s' ${url}`);
         console.log(stdout, stderr);
         var info = stdout.split(":", 1);
         name = info[0].substr(10);

         return resolve(name);
      } catch (error) {
         console.error('## ', error);
         return reject(error);
      }
   });
}

function SHFfmpeg(name) {
   console.log('SHFfmpeg');
   return new Promise(async (resolve, reject) => {
      try {
         const {
            stdout,
            stderr
         } = await exe(`ffmpeg -ss 10 -t 60 -i ${process.env.APP_STORAGE}/${name}.mp3 ${process.env.APP_STORAGE}/${name}_splitted.mp3`);
         console.log(stdout);
         return resolve(true);
      } catch (error) {
         console.error(error);
         return reject(false);
      }
   });
}

function recognizeByAuddIO(name) {
   console.log('recognizeByAuddIO');
   return new Promise(async (resolve, reject) => {
      try {
         const file_path = path.join(__dirname, `../../${process.env.APP_STORAGE}/${name}_splitted.mp3`);
         let file = await fs.createReadStream(file_path);
         var api = await APIAuddIO(file, file_path);
         if (api.status !== 'success') throw new Error(api.error.error_message);
         else if (api.result === null) throw new Error('Artist not found..');
         let artist = api.result.artist;
         console.log(artist);
         return resolve(artist);

      } catch (error) {
         console.error('!!', error);
         return reject(error);
      }
   });
}

function APIAuddIO(file, file_path) {
   console.log('APIAuddIO');
   // Setting URL and headers for request
   var options = {
      method: 'POST',
      url: 'https://api.audd.io/',
      qs: {
         api_token: 'test'
      },
      headers: {
         'Content-Type': 'application/x-www-form-urlencoded'
      },
      formData: {
         file: {
            value: file,
            options: {
               filename: file_path,
               contentType: null
            }
         },
         return: 'timecode'
      }
   };
   // Return new promise 
   return new Promise(function (resolve, reject) {
      // Do async job
      request(options, function (err, resp, body) {
         if (err) {
            console.error(err);
            return reject(err);
         } else {
            return resolve(JSON.parse(body));
         }
      })
   })
}

module.exports = {
   handle,
   deleteFiles
}